#include <vector>
#include <iostream>
#include <algorithm>
#include <climits>
#include <cmath>
#include <ctime>
#include <unordered_map>
using namespace std;

/*
   difference policies for cost of moving
case1 : cost of moving k sized chunk = k
case2 : cost of moving k sized chunk is: 
K if k< K
k otherwise
case3 : cost of moving k size chunk is:
ceil(k/K) 
*/

/*
Sample output:

starting disk state
13 3 9 8 0 15 0 6 16 12 0 7 1 10 5 11 2 14 0 4
files information
file 1 start block to end block 1 8
file 2 start block to end block 9 16

running random search with time limit 5secs
random search minimum cost defragmented disk state
9 10 11 12 13 14 15 16 1 2 3 4 5 6 7 8 0 0 0 0
****** result from random search is 20

exhaustive search min so far 24 count 1
exhaustive search min so far 24 count 2
exhaustive search min so far 24 count 3
exhaustive search min so far 24 count 4
exhaustive search min so far 23 count 5
exhaustive search min so far 20 count 6
exhaustive search min so far 20 count 7
exhaustive search min so far 20 count 8
exhaustive search min so far 20 count 9
exhaustive search min so far 20 count 10
exhaustive search minimum cost defragmented disk state
9 10 11 12 13 14 15 16 1 2 3 4 5 6 7 8 0 0 0 0
****** result from exhaustive search is 20
****** result from random search is 20

*/

#define CASE 1

class Defragment {
    public:
        bool debug = false;
        
        /* This function returns the minimum number of moves necessary for
           transforming disk state v1 to v2. This assumes moving each block 
           is 1 and for a chunk of k blocks the cost is k.
        */
        int min_moves_between_disk_states(vector<int>& v1, vector<int>& v2){
            vector<bool> vis;
            vector<int> m;
            clock_t begin = clock();

            m.resize(v1.size(),-1);
            vis.resize(v1.size(),false);
            int res = 0;
            for(int i=0;i<v2.size();i++){
                m[v2[i]] = i;
            }
            for(int i=0;i<v1.size();i++){
                vis[i] = true;
                if(v1[i] != 0 && m[v1[i]] != i){
                    int k = i;
                    int cycle_len = 0;
                    do{
                        cycle_len++;
                        vis[k] = true;
                        k = m[v1[k]];
                    }while(v1[k] !=0 && !vis[k]);
                    res += cycle_len;
                }
            }
            if(debug){
                cout << "min moves is res " << res << "\n";
                for(auto k: v1) cout << k << " ";
                cout << "\n";
                for(auto k: v2) cout << k << " ";
                cout << "\n";
            }
            clock_t end = clock();
            double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
            if(debug)
                cout << "min distance bet. states elapsed secs " << elapsed_secs << "\n";
            return res;
        }

        /* This function counts the various chunks that have been moved between
           disk states v1 and v2 and returns an adjustment count to adjust 
           the result for policies case 2 and 3. 
        */
        int count_additional_cost(vector<int>& v1, vector<int>& v2, int k){
            int res = 0;
            if( CASE != 1){
                int chunksize=1;
                bool unmoved_chunk = false;
                if(debug){
                    cout << "add cost \n";
                    for(auto k: v1) cout << k << " ";
                    cout << "\n";
                    for(auto k: v2) cout << k << " ";
                    cout << "\n";
                }
                for(int i=0;i<v1.size();i++){
                    if(v1[i] ==0) continue;
                    if( (i!=(v1.size()-1)) && v1[i]+1 == v1[i+1] ){
                        if(v1[i] == v2[i]) unmoved_chunk = true;
                        chunksize++;
                    }else{
                        if(debug)
                            cout << "chunk ending at " << i << " " << chunksize << "unmoved " << unmoved_chunk << "\n";
                        if(CASE == 2){
                            if(chunksize < k && !unmoved_chunk){
                                res += (k-chunksize);
                            }
                        }else{
                            if(!unmoved_chunk){
                                if(debug)
                                    cout << "chunk size " << chunksize << " k " << k << " ceil(chunksize/k) " << ceil(chunksize/k) << " " << chunksize << "\n";
                                res += ceil((float)chunksize/k)-chunksize;
                            }
                        }
                        chunksize = 1;
                        unmoved_chunk = false;
                    }
                }
            }
            if(debug)
                cout << "add cost res " << res << "\n";
            return res;
        }


        /* This function iterates over all possible defragmented disk states and calculates the minimum 
           number of moves to transform to each of them. It returns the minimum of all such minimums.
        */
        int min_moves_to_defragment_exhaustive_search(vector<int>& v1, vector<pair<int,int>> file_endings, int kval){
            int minval = INT_MAX;
            sort(file_endings.begin(), file_endings.end(), [](const pair<int,int>& x, pair<int,int>& y){
                return x.first < y.first;
            });
            vector<int> min_state;
            int numblocks = file_endings.back().second;
            int count = 0;
            do{
                for(int i=0;i<=(v1.size()-numblocks);i++){
                    int y = i;
                    vector<int> v2;
                    v2.resize(v1.size(),0);
                    for(auto k: file_endings){
                        for(int j=k.first;j<=k.second;j++)
                            v2[y++] = j;
                    }
                    if(debug){
                        cout << "exhaustive search working on sate \n";
                        for(auto k: v2) cout << k << " ";
                        cout << "\n";
                    }
                    count++;
                    int r = min_moves_between_disk_states(v1,v2) + count_additional_cost(v1,v2,kval);
                    if(r < minval){
                        min_state = v2;
                        minval = r;
                    }
                    if(count%100){
                        cout << "exhaustive search min so far " << minval << " count " << count << "\n";
                    }
                }
            }while(next_permutation(file_endings.begin(), file_endings.end(), [](const pair<int,int>& x, const pair<int,int>& y){
                return x.first < y.first;
                }));
            cout << "exhaustive search minimum cost defragmented disk state \n";
            for(auto k: min_state) cout << k << " ";
            cout << "\n";
            return minval;
        }

        /* This function calculates the index of the largest chunk in each of the files
        *  and sorts all files based on this index position. It then generates a defragmented
        *  disk state that has files in this order. This is a heuristic to generate a optimal
        *  defragmented disk state.
        */
        vector<pair<int,int>>  heuristic_optimial_state(vector<int>& v1, vector<pair<int,int>>& files){
                int res = 0, chunksize=1;
                vector<pair<int,int>> neworder = files;
                vector<vector<int>> locs;
                //locs vector holds the index in V1 , size of largest chunk, index of file in files vector for each file
                locs.resize(files.size(),{-1,0,-1});
                for(int i=0;i<v1.size();i++){
                    if(v1[i] ==0) continue;
                    if( (i!=(v1.size()-1)) && v1[i]+1 == v1[i+1] ){
                        chunksize++;
                    }else{
                        if(debug)
                            cout << "chunk ending at " << i << " " << chunksize << "\n";
                        //just considering that whole chunk belongs to the file where first block belongs
                        //TODO: make it more accurate later
                        auto k = lower_bound(files.begin(),files.end(),make_pair(i-chunksize,i+chunksize-1),[](const pair<int,int>& x, const pair<int,int>& y){ return x.second < y.first; });
                        int ind = k-files.begin()-1;
                        if(ind < 0) ind = 0;
                        if( locs[ind][1] < chunksize){
                            locs[ind][1] = chunksize;
                            locs[ind][0] = i-chunksize;
                            locs[ind][2] = ind;
                        }
                    }
                }
                sort(locs.begin(), locs.end(), [](const vector<int>& x, const vector<int>& y){
                        return x[0] < y[0];
                        });
                for(int i=0; i< locs.size();i++){
                    neworder[i] = files[locs[i][2]];
                }
                return neworder;
        }
        
        /* This function starts from the heuristic estimate of the optimal defragmented disk state and 
         * then randomly moves to another defragmented disk state that has different free space between files
         * or one random swap in the order of files.
         *  TODO: also include exploring differing amounts of free space in between files 
        */
        int min_moves_to_defragment_random_search(vector<int>& v1, vector<pair<int,int>> file_endings, int kval, int timelimit){
            clock_t begin = clock();
            vector<pair<int,int>> sorted_file_endings = file_endings;
            file_endings = heuristic_optimial_state(v1,sorted_file_endings);
            int minval = INT_MAX;
            int minstartindex = -1;
            vector<int> min_state;
            vector<pair<int,int>> min_file_endings;
            int numblocks = sorted_file_endings.back().second;
            do{
                int i=rand()%(v1.size()-numblocks+1);
                vector<int> v2;
                v2.resize(v1.size(),0);
                for(auto k: file_endings){
                    for(int j=k.first;j<=k.second;j++,i++)
                        v2[i] = j;
                }
                if(debug){
                    cout << "random search working on sate \n";
                    for(auto k: v2) cout << k << " ";
                    cout << "\n";
                }
                int r = min_moves_between_disk_states(v1,v2) + count_additional_cost(v1,v2,kval);
                if(r< minval){
                    minval = r;
                    minstartindex = i;
                    min_file_endings = file_endings;
                    min_state = v2;
                }
                clock_t end = clock();
                double elapsed_secs = (double(end - begin) / CLOCKS_PER_SEC);
                if(elapsed_secs > timelimit){ break;};
                //swap the last file with some other random file to explore another defragmented state.
                int len = (file_endings.size()-1);
                if(len > 0){
                    int swapind = rand()%(len);
                    swap(file_endings[swapind],file_endings[len]);
                }
            }while(true);
            cout << "random search minimum cost defragmented disk state \n";
            for(auto k: min_state) cout << k << " ";
            cout << "\n";
            return minval;
        }
};

int main(){
    Defragment s;
    srand(time(NULL));
    //vector<int> v1 = {0,0,1,2,0,0,0,4,5,0,0,3};
    //vector<int> v1 = {0,0,1,2,0,4,5,0,0,3,0};
    //vector<int> v2 = {0,0,1,2,3,4,5,0,0,0,0,0};

    //vector<int> v1 = {0,0,101,102,0,0,0,104,105,0,0,103,0,0,0,0,0,0,0,0,0,0,0,0};
    //vector<int> v1 = {0,0,102,101,0,0,0,105,104,0,0,103,0,0,0,0,0,0,0,0,0,0,0,0};
    //auto res = s.min_moves_between_disk_states(v1,v2);
   
    //A real disk state must be first transformed in such a way that 
    //blocks in all files are numbered sequentially.
    //For example, if real disk has: { File1Block1 Empty File2Block1 Empty File1Block2 }
    //then the input will be { 1 0 3 0 2 } and file endings will be {1-2,3-3}
    //Generate a random fragmented disk state
    int total_disk_blocks = 20, occupied_disk_blocks=16;
    vector<int> v1;
    v1.resize(total_disk_blocks,0);
    for(int i=1;i<=(occupied_disk_blocks);i++){
        int t = rand()%total_disk_blocks;
        while(v1[t] != 0){
            t = rand()%total_disk_blocks;
        }
        v1[t] = i;
    }
    //starting and ending block number of each file.
    vector<pair<int,int>> file_endings;
    file_endings.push_back(make_pair(1,occupied_disk_blocks/2));
    file_endings.push_back(make_pair((occupied_disk_blocks/2)+1,occupied_disk_blocks));
    
    cout << "starting disk state \n";
    for(auto k: v1) cout << k << " ";
    cout << "\n";

    sort(file_endings.begin(), file_endings.end(), [](const pair<int,int>& x, const pair<int,int>& y){
            return x.first < y.first;
            });

    cout << "files information \n";
    for(int i=0;i<file_endings.size();i++){
        cout << "file " << i+1 << " start block to end block " << file_endings[i].first << " " << file_endings[i].second << "\n";
    }

    int timelimit = 5;
    cout << "running random search with time limit " << timelimit << "secs \n";
    auto res2 = s.min_moves_to_defragment_random_search(v1,file_endings,5,timelimit/*time limit in seconds*/);
    cout << "****** result from random search is " << res2 << "\n";
    auto res1 = s.min_moves_to_defragment_exhaustive_search(v1,file_endings,5);
    cout << "****** result from exhaustive search is " << res1 << "\n";
    cout << "****** result from random search is " << res2 << "\n";

    //cout << "elapsed secs " << elapsed_secs << "\n";
    //cout << "result " << res << "\n";
    return 0;
}
